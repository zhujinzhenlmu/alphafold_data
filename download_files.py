import os,re
base_folder = os.path.expanduser('~')+'/af_data/'
base_url = 'http://ftp.cbi.pku.edu.cn/psp/'
bucket_base = 's3://AF_data_jinzhen'
if not os.path.isdir(base_folder):
    os.mkdir(base_folder)

def file_consistent(url,file_name):
    remote_size = int(os.popen('curl -sI %s | grep -i Content-Length' % url).readlines()[0].strip().split()[1])
    ls_command = 'aws s3 ls %s/%s' % (bucket_base, file_name)
    bucket_content = os.popen(ls_command).readlines()
    bucket_size=0
    if len(bucket_content)>0:
        bucket_size = int(bucket_content[0].split()[2])
    if remote_size != bucket_size:
        print('size diff with ', remote_size, bucket_size)
    return remote_size == bucket_size

def download_item(file_name):
    target_folder_name = '/'.join(file_name.split('/')[:-1])
    remote_url = '%s/%s' % (base_url, file_name)
    if file_consistent(remote_url ,file_name):
        print(file_name,'already exists')
        return
    command_line = 'wget -c %s/%s -P %s/%s' % (base_url, file_name,base_folder,target_folder_name)
    os.system(command_line)
    
def upload_item(file_name):
    upload_command = 'aws s3 cp %s/%s %s/%s' % (base_folder,file_name,bucket_base,file_name)
    print(upload_command)
    os.system(upload_command)
    rm_command = 'rm %s/%s' % (base_folder,file_name)
    print(rm_command)
    os.system(rm_command)
    
contents = [item.strip() for item in open('files.txt').readlines()]

import sys

if __name__ == '__main__':
    if len(sys.argv)<3:
        sys.exit('please run this script as python xxx.py start_idx end_idx')
    for i in range(int(sys.argv[1]),int(sys.argv[2])):
        download_item(contents[i])
        upload_item(contents[i])