base_url = 'http://ftp.cbi.pku.edu.cn/psp/'

import requests
from requests.exceptions import HTTPError
from bs4 import BeautifulSoup


def get_file_list(url):
    return_dict = {}
    try:
        response = requests.get(url)

        # If the response was successful, no Exception will be raised
        response.raise_for_status()
    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')  # Python 3.6
    except Exception as err:
        print(f'Other error occurred: {err}')  # Python 3.6
    else:
        response.encoding = 'utf-8' # Optional: requests infers this internally
        soup = BeautifulSoup(response.text, 'lxml')
        links = soup.find_all('a')
        for link in links:
            href = link['href']
            if 'Parent Directory' not in link.getText():
                if href[-1] == '/':
                    return_dict[href] = get_file_list(url+href)
                else:
                    return_dict[href] = 'file'
        forms = soup.find_all('form')
    return return_dict

def write_dict(dict,base_path,file):
    for key in dict:
        if dict[key]=='file':
            file.write(base_path+key+'\n')
        else:
            write_dict(dict[key],base_path+key,file)
    
all_files = get_file_list(base_url)
files_contents = '/home/PJLAB/zhujinzhen/projects/alphafold_data/files.txt'

with open(files_contents,'a+') as f:
    write_dict(all_files,'',f)
